import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

export default defineConfig(({ mode }) => {
	return {
		base: '/',
		plugins: [
			vue(),
		],
		resolve: {
			alias: {
				'@': path.resolve(__dirname, './src'),
			},
		},
		css: {
			preprocessorOptions: {
				stylus: {
					imports: [path.resolve(__dirname, 'src/assets/css/variables.styl')],
				}
			}
		}
	}
})
