import { createRouter, createWebHistory } from 'vue-router';
import Home from "@/components/home.vue"
import Game from "@/components/game.vue"

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { name: 'home', path: '/', component: Home },
    { name: 'game', path: '/game/:slug', component: Game },
  ]
})

export default router;